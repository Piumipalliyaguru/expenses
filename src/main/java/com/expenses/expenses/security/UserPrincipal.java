package com.expenses.expenses.security;

import com.expenses.expenses.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserPrincipal implements UserDetails {
    private Long id;
   /* private String firstName;
    private String lastName;*/

    private String username;

    @JsonIgnore
    private String email;

    @JsonIgnore
    private String password;

    private String role;

    private Collection<? extends GrantedAuthority> authorities;

    @SuppressFBWarnings({"URF_UNREAD_FIELD", "URF_UNREAD_FIELD"})
    public UserPrincipal(Long id, String username, String email, String password, String role, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
       /* this.firstName = firstName;
        this.lastName = lastName;*/
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
        this.authorities = authorities;
    }

    public static UserPrincipal create(User user) {
        List<GrantedAuthority> authorities = user.getRole().getPermissions().stream().map(permission ->
                new SimpleGrantedAuthority(permission.getPermission().name())).collect(Collectors.toList());

        return new UserPrincipal(
                user.getId(),
               /* user.getFirstName(),
                user.getLastName(),*/
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getRole().getName().name(),
                authorities

        );
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

  /*  public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }*/


    public String getEmail() {
        return email;
    }

    public String getRole() {
        return role;
    }
}
