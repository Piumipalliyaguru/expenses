package com.expenses.expenses.model;

import com.expenses.expenses.model.audit.DateAudit;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@SuppressFBWarnings("SE_BAD_FIELD")
@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "username"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
@Getter @Setter @NoArgsConstructor
public class User extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

/*    @NotBlank
    @Size(max = 40)
    private String firstName;

    @NotBlank
    @Size(max = 15)
    private String lastName;*/

    @NotBlank
    @Size(max = 15)
    private String username;

    @NaturalId
    @NotBlank
    @Size(max = 40)
    @Email
    private String email;

    @NotBlank
    @Size(max = 300)
    private String password;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")
    private Role role;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private UserAccount account;

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    @Fetch(FetchMode.SELECT)
    private List<Transaction> transactions = new ArrayList<>();

    //will propagate (cascade) all EntityManager operations (PERSIST, REMOVE, REFRESH, MERGE, DETACH) to the relating entities.
    //To load it together with the rest of the fields (i.e. eagerly), or
    //To load it on-demand (i.e. lazily) when you call the university's getStudents() method.
    //orphanRemoval is an entirely ORM-specific thing.
    // It marks "child" entity to be removed when it's no longer referenced from the "parent" entity
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    @Fetch(FetchMode.SELECT) //   Fetch eagerly, using a separate select.
    private List<Budget> budget = new ArrayList<>();

    public User(String username, String email, String password) {
       /* this.firstName = firstName;
        this.lastName = lastName;*/
        this.username = username;
        this.email = email;
        this.password = password;
    }

}
