package com.expenses.expenses.model;

import com.expenses.expenses.model.audit.UserDateAudit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "report")
@Getter
@Setter
@NoArgsConstructor
public class Report extends UserDateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "reportType_id")
    private ReportType reportType;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "report_transactions",
            joinColumns = @JoinColumn(name = "report_id"),
            inverseJoinColumns = @JoinColumn(name = "transaction_id"))
    private Set<Transaction> transactions = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "report_users",
            joinColumns = @JoinColumn(name = "report_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> users = new HashSet<>();

    public Report(int id, String name, String description, ReportType reportType, Set<Transaction> transactions) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.reportType = reportType;
        this.transactions = transactions;
    }

}
