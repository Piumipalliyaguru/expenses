package com.expenses.expenses.model;

public enum RoleName {
    SUPER_ADMIN,
    ADMIN,
    USER

}
