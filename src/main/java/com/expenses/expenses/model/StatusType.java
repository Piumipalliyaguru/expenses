package com.expenses.expenses.model;

public enum StatusType {
    ACTIVE,
    HOLD,
    DE_ACTIVE,
    DELETED
}
