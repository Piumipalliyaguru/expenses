package com.expenses.expenses.model;

import com.expenses.expenses.model.audit.DateAudit;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "account")
public class UserAccount extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //will propagate (cascade) all EntityManager operations (PERSIST, REMOVE, REFRESH, MERGE, DETACH) to the relating entities.
    //To load it together with the rest of the fields (i.e. eagerly), or
    //To load it on-demand (i.e. lazily) when you call the university's getStudents() method.
    //orphanRemoval is an entirely ORM-specific thing.
    // It marks "child" entity to be removed when it's no longer referenced from the "parent" entity
    @OneToMany(
    mappedBy = "account",
    cascade = CascadeType.ALL,
    fetch = FetchType.EAGER,
    orphanRemoval = true
            )
    @Size(min = 1)
    @Fetch(FetchMode.SELECT) //   Fetch eagerly, using a separate select.
    private List<User> users = new ArrayList<>();

    /*@Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusType status;*/
}
