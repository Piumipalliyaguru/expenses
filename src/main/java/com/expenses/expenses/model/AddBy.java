package com.expenses.expenses.model;

public enum AddBy {
    USER,
    SYSTEM
}
