package com.expenses.expenses.model;

public enum PermissionName {
    MANAGE_USER,
    VIEW_USER
}
