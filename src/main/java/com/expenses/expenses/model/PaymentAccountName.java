package com.expenses.expenses.model;

public enum PaymentAccountName {
    CASH,
    CREDIT,
    DEBIT
}
