package com.expenses.expenses.model;

public enum TransActionName {
    INCOME,
    EXPENSES,
    TRANSFER
}
