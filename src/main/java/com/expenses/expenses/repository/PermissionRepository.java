package com.expenses.expenses.repository;

import com.expenses.expenses.model.Permission;
import com.expenses.expenses.model.PermissionName;
import com.expenses.expenses.model.Role;
import com.expenses.expenses.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {

    Optional<Permission> findByPermission(PermissionName permissionName);

}
